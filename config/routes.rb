Rails.application.routes.draw do
  resources :dogs, only: [:index]

  root to: redirect('/dogs')
end
