FROM ruby:2.2.0
RUN apt-get update -qq && apt-get install -y build-essential libpq-dev
RUN mkdir /rails_app
WORKDIR /rails_app
ADD Gemfile /rails_app/Gemfile
RUN bundle install
ADD . /rails_app